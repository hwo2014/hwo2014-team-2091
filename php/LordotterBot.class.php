<?php
define('MAX_LINE_LENGTH', 1024 * 1024);

require_once 'TrackInspect.php';

class LordotterBot {
	protected $sock, $debug;

    protected $track;

	function __construct($host, $port, $botname, $botkey, $debug = TRUE) {
		$this->debug = $debug;
		$this->connect($host, $port, $botkey);
		$this->write_msg('join', array(
			'name' => $botname,
			'key' => $botkey
		));
	}

	function __destruct() {
		if (isset($this->sock)) {
			socket_close($this->sock);
		}
	}

	protected function connect($host, $port, $botkey) {
		$this->sock = @ socket_create(AF_INET, SOCK_STREAM, SOL_TCP);
		if ($this->sock === FALSE) {
			throw new Exception('socket: ' . socket_strerror(socket_last_error()));
		}
		if (@ !socket_connect($this->sock, $host, $port)) {
			throw new Exception($host . ': ' . $this->sockerror());
		}
	}

	protected function read_msg() {
		$line = @ socket_read($this->sock, MAX_LINE_LENGTH, PHP_NORMAL_READ);
		if ($line === FALSE) {
			$this->debug('** ' . $this->sockerror());
		} else {
			$this->debug('<= ' . print_r(json_decode($line, true) ) );
		}
		return json_decode($line, TRUE);
	}

	protected function write_msg($msgtype, $data) {
		$str = json_encode(array('msgType' => $msgtype, 'data' => $data)) . "\n";
		$this->debug('=> ' . rtrim($str));
		if (@ socket_write($this->sock, $str) === FALSE) {
			throw new Exception('write: ' . $this->sockerror());
		}
	}
	
	protected function sockerror() {
		return socket_strerror(socket_last_error($this->sock));
	}
	
	protected function debug($msg) {
		if ($this->debug) {
			echo $msg, "\n";
		}
	}
	
	public function run() {
		while (!is_null($msg = $this->read_msg())) {
			switch ($msg['msgType']) {
                case 'gameInit':
                    $track = new TrackInspect($msg['data']);
                    $this->track = $track->inspect();
                    $this->write_msg('ping', null);
                    break;
                case 'carPositions':
                    $piece = $this->track[ $msg['data'] [0] ['piecePosition'] ['pieceIndex'] + ($track->getCountPieces() * $msg['data'] [0] ['piecePosition'] ['lap']) ];
                    $trottle =  $piece['throttle'];
                    $lengthNextBend = $piece['lengthNextBend'];
                    $lengthLastBend = $piece['lengthLastBend'];

                    if ( $msg['data'] [0] ['angle'] >= 55 || $msg['data'] [0] ['angle'] <= -55 ) {
                        $trottle *= 0.3;
                    }
                    elseif ( $msg['data'] [0] ['angle'] >= 53 || $msg['data'] [0] ['angle'] <= -53 ) {
                        $trottle *= 0.5;
                    }
                    elseif ( $msg['data'] [0] ['angle'] >= 52 || $msg['data'] [0] ['angle'] <= -52 ) {
                        $trottle *= 0.57;
                    }
                    elseif ( $msg['data'] [0] ['angle'] >= 50 || $msg['data'] [0] ['angle'] <= -50 ) {
                        $trottle *= 0.69;
                    }
                    elseif ( $msg['data'] [0] ['angle'] >= 45 || $msg['data'] [0] ['angle'] <= -45 ) {
                        $trottle *= 0.75;
                    }
                    elseif ( $msg['data'] [0] ['angle'] >= 40 || $msg['data'] [0] ['angle'] <= -40 ) {
                        $trottle *= 0.85;
                    }
                    elseif ( $msg['data'] [0] ['angle'] >= 35 || $msg['data'] [0] ['angle'] <= -35 ) {
                        $trottle *= 0.93;
                    }
                    elseif ( $msg['data'] [0] ['angle'] >= 30 || $msg['data'] [0] ['angle'] <= -30 ) {
                        $trottle *= 0.98;
                    }
                    if ($lengthLastBend < 200 ) {
                        if ( $msg['data'] [0] ['angle'] <= 1 || $msg['data'] [0] ['angle'] >= -1 ) {
                            $trottle *= 1.17;
                            if ($trottle > 1) {
                                $trottle = 1;
                            }
                        }
                        elseif ( $msg['data'] [0] ['angle'] <= 5 || $msg['data'] [0] ['angle'] >= -5 ) {
                            $trottle *= 1.05;
                            if ($trottle > 1) {
                                $trottle = 1;
                            }
                        }
                    }

                    $this->write_msg('throttle', $trottle);
//                    echo "\n" . $msg['data'] [0] ['piecePosition'] ['pieceIndex'] + ($track->getCountPieces() * $msg['data'] [0] ['piecePosition'] ['lap'])."\n";
                    break;
				case 'join':
				case 'yourCar':
				case 'gameStart':
				case 'crash':
				case 'spawn':
				case 'lapFinished':
				case 'dnf':
				case 'finish':
				default:
					$this->write_msg('ping', null);
			}
		}
	}
}
?>
