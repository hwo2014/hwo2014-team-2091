<?php
/**
 * Created by PhpStorm.
 * User: Thomas Ott
 * Date: 16.04.14
 * Time: 19:55
 */

class TrackInspect {

    /**
     * @var ArrayIterator
     */
    protected $pieces;

    protected $laps;

    protected $lastBend;

    protected $nextBend;

    /**
     * @var int
     */
    protected $countPieces;

    public function __construct($data)
    {
        $this->laps = $data['race']['raceSession']['laps'];
        $this->lastBend = 0;
        $this->nextBend = 0;

        $this->pieces = new ArrayIterator();


        for ($i = 0; $i < $this->laps; $i++) {
            foreach ($data['race']['track']['pieces'] as $piece) {
                $this->pieces->append($piece);
            }
        }
        $this->countPieces = count($data['race']['track']['pieces']);
    }

    public function inspect()
    {
        $this->firstStep();
        $this->secondStep();
        $this->thirdStep();

        return $this->pieces;
    }

    protected function firstStep()
    {
        foreach ($this->pieces as $key => &$piece) {

            if ( !isset($piece['radius']) ) {
                $piece['throttle'] = 1.0;
                $piece['isBend'] = false;
            }
            //          Rightbend                                           Leftbend
            elseif ( ( $piece['radius'] <= 100 && $piece['angle'] <= 45 ) || ( $piece['radius'] >= -100 && $piece['angle'] >= -45) ) {
                $piece['throttle'] = 0.7;
                $piece['isBend'] = true;
            }
            //          Rightbend                                           Leftbend
            elseif ( ( $piece['radius'] <= 200 && $piece['angle'] <= 22 ) || ( $piece['radius'] >= -200 && $piece['angle'] >= -22) ) {
                $piece['throttle'] = 0.9;
                $piece['isBend'] = true;

            }
        }
    }

    /**
     * Count length to the next bend
     * Count length from last bend
     */
    protected function secondStep()
    {
        $nextBend = 0;
        $lastBend = 0;
        while ($nextBend !== false) {
            $nextBend = $this->getNextBend($nextBend);
            for ($i = $lastBend; $i < $nextBend; $i++) {
                $this->pieces[$i]['lengthNextBend'] = 0;
                for ($ii = $i; $ii < $nextBend; $ii++) {
                    $this->pieces[$i]['lengthNextBend'] += $this->pieces[$ii]['length'];
//                    echo $i . ' ' .$this->pieces[$i]['lengthNextBend']."\n";
                }
//                echo "---\n";
            }
//echo "------\n";
            for ($i = $nextBend; $i > $lastBend; $i--) {
                $this->pieces[$i]['lengthLastBend'] = $this->pieces[$i]['length'];
                for ($ii = $i; $ii > $lastBend; $ii--) {
                    $this->pieces[$i]['lengthLastBend'] += $this->pieces[$ii]['length'];
//                    echo $i . ' ' .$this->pieces[$i]['lengthLastBend']."\n";
                }
//                echo $i . ' ' .$this->pieces[$i]['lengthLastBend']."\n";
//                echo "+++\n";
            }


            $lastBend = $nextBend;


//            echo "next Bend: " .$nextBend . "\n";
        }
//        var_dump($this->pieces);
//        die();

    }

    protected function thirdStep()
    {
        foreach ($this->pieces as $key => &$piece) {
            if (!$piece['isBend']) {
                $nextPiece = $this->pieces[$key + 2];
                if ( $nextPiece['isBend'] ) {
                    if ($this->pieces[$key]['lengthLastBend'] >= 100)
                        $piece['throttle'] = ($nextPiece['throttle'] * 0.5);
                }
            }
        }
    }


    private function getNextBend($i)
    {
        $this->pieces->seek($i);
        do
        {
            $this->pieces->next();
            $piece = $this->pieces->current();
            if ( !$piece['isBend'] ) {
                continue;
            }

            return $this->pieces->key();
        }
        while($this->pieces->valid());

        return false;
    }

    public function getCountPieces()
    {
        return $this->countPieces;
    }
}